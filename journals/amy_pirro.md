# Week 4: 1/30/24 - 2/5/24

- frontend auth implemented (mob programming, Tay driving)
  - set up AuthProvider
- frontend:
  - installed react-router-dom, set up BrowserRouter and Routes, installed Tailwind and Flowbite (MP, Tay driving)
  - login page + form React component
  - added Potlucky logo to new assets folder
  - added PrivateRoutes component and setup in app.js
  - nav component
  - implemented UserProvider and installed library to decode token to get logged-in user's information from decoded token
  - UserProfile component, in progress
  - mob programming troubleshooting and testing: Tay's create event form, Matt's signup form, Daniel's create item form and event details page
- backend:
  - attendees endpoints
    - POST attendee (pair programming, Matt driving)
    - GET attendees by event (me)
    - DELETE attendee (Matt)
    - GET events by attendee (PP, Matt driving)
  - event_categories endpoints
    - GET categories by event (me)
    - DELETE category from event (PP, Tay driving)
    - POST category to event (Tay)
- unit tests, everybody wrote one and they all pass
- deployment:
  - database deployed (MP, me driving)
  - backend deployed (MP, me driving)


# Week 3: 1/23/24 - 1/29/24

- users endpoints:
    - GET one user (mob programming, Matt driving)
    - DELETE user (MP, Tay driving), and later I fixed so users can only delete their own account and nobody else's
    - PUT edit user (MP, Daniel driving)
    - GET all users (MP, me driving)

- categories endpoints
    - GET all categories (MP, Matt driving)
    - GET one category (me)

- items endpoints
    - I created Pydantic models for ItemIn, ItemOut
    - POST create item (pair programming, Matt and me, Matt driving), and later I refactored so user_id comes from auth and doesn't need to be entered in request body
    - GET items by event (me), included writing table joins to return Category and BasicUserOut objects
    - DELETE item (pair programming, Matt driving)
    - PUT edit item (Matt)
    - GET one item (Matt)
    - cleaned up some error handling, deleted any print statements, formatted, tested all endpoints multiple times, merged with main-developer, tested again

- events endpoints
    - Tay and Daniel working on these

# Week 2: 1/16/24 – 1/22/24

### Mon 1/22
- mob programming, Tay driving: finished up authentication by adding GET token endpoint
  - fixed a couple parameter variable names to what jwtdown was expecting and everything worked
  - merged
- mob programming, me driving: added 5 tables to db in 002 migration file (events, categories, event_categories, items, attendees)
- created gitlab issues for the rest of the user endpoints

### Fri 1/19
- mob programming, Tay driving: authentication
  - many 500 errors
  - eventually with SEIR help (shoutout Ian & Alex) we discovered some parameter variable names could NOT be changed because jwtdown was expecting certain things; had incorrect pydantic model as output for at least one function; had a "helper" function that was not returning a pydantic model at all when it needed to be

### Thurs 1/18
- mob programming, Daniel driving: created first endpoint, POST user
- mob programming, Tay driving: started implementing jwtdown-fastapi, got login and logout endpoints

### Weds 1/17
- mob programming, me driving: added PostgreSQL database to project, merged, we all created volume and rebuilt
- mob programming, Matt driving: inserted Users table into db
  - Tay discovered 'user' is keyword, named table 'Users' instead
- decided to have extra layer of security on merge requests and are using main-developer branch as main

### Tues 1/16
- design review
- created wireframe for 'create event' form view
- reviewed and edited endpoint plans


# Week 1: 1/8/24–1/12/24
- each of us cloned repo
- created MVP features list as a team
- created wireframes as a team
- created api endpoints as a team
- I created draft Entity Relationship Diagram(ERD) in [Excalidraw](https://excalidraw.com/#room=7c7580a2e5102bc36b28,urZelZBflxzEh8wBeR_LWA)
