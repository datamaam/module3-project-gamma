Entry 4 Jan-30 -- Feb-5
Made the event_form frontend got everything to work on it. then made the event_category endpoints with also making an unit test for category. The group has merge all their unit test and other part they where working on. we came together for a code along with deployment got the db and backend deployed. we have have merge proofed and made sure main is all good to go.

Entry 3 Jan-23 -- Jan-29
Made a Post, Delete and get endpoint for event making sure all of them work and connected to one another. created and fixed issues. group split into a event team and  item team. giving everyone a chance to make more endpoints

Entry 2 Jan-16 -- Jan-22
Made a POST for users. also letting them log in and out with authentication. edit and created more issues. Included 5 more tables. Made sure everyone has the most updated data. we made a extra branch for more leeway room. Everyone was in the driver seat this week.

Entry 1 Jan-8 -- Jan-12
Creating the project "potlucky" so all members can clone and work on the project together. We have made the mvp which iterates what we want to be done for our project. We also made wire-frames which showcase how we want the project to look like. We also made the api-endpoints that shows how we want our apis to run and how they should look.
