from fastapi import (
    Depends,
    APIRouter,
    HTTPException,
)
from authenticator import authenticator
from queries.event_queries import EventRepo
from queries.eventcat_queries import (
    EventCategory,
    EventCategoryRepo,
    EventCategoriesOut,
)

router = APIRouter(tags=["Event Categories"])


@router.post(
    "/api/events/{event_id}/{category_id}", response_model=EventCategory
)
def add_category_to_event(
    event_id: int,
    category_id: int,
    repo: EventCategoryRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(event_id, category_id)


@router.delete("/api/events/{event_id}/{category_id}")
def delete_categories(
    event_id: int,
    category_id: int,
    repo: EventCategoryRepo = Depends(),
    event_repo: EventRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    one_event = event_repo.get_event(event_id)
    if one_event.owner_id == account_data["id"]:
        return repo.delete(event_id, category_id)
    else:
        raise HTTPException(
            status_code=400, detail="You can not delete this category"
        )


@router.get(
    "/api/events/{event_id}/categories", response_model=EventCategoriesOut
)
def get_categories_by_event(
    event_id: int,
    repo: EventCategoryRepo = Depends(),
):
    return repo.get_all(event_id)
