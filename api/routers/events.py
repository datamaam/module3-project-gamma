from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    # Request,
)

# from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from typing import Union, Optional, List
from queries.event_queries import (
    EventRepo,
    EventIn,
    Error,
    #  EventOut,
    DuplicateEventError,
    EventWithOutCategories,
)

router = APIRouter(tags=["Events"])


@router.delete("/api/events/{event_id}", response_model=bool)
def delete_event(
    event_id: int,
    repo: EventRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    event = repo.get_event(event_id)

    if event and account_data and event.owner_id == account_data["id"]:
        return repo.delete(event_id)
    else:
        return False


@router.post("/api/events", response_model=EventWithOutCategories)
def create_event(
    event: EventIn,
    repo: EventRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    owner_id = account_data["id"]
    try:
        new_event = repo.create(event, owner_id)
        return new_event
    except DuplicateEventError as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
        )


@router.get(
    "/api/events/public",
    response_model=Union[List[EventWithOutCategories], Error],
)
def get_public_events(
    repo: EventRepo = Depends(),
):
    return repo.get_all_public()


@router.get(
    "/api/events/{event_id}", response_model=Optional[EventWithOutCategories]
)
def get_event(
    event_id: int,
    response: Response,
    repo: EventRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> EventWithOutCategories:
    event = repo.get_event(event_id)
    if event is None:
        response.status_code = 404
    return event


@router.put(
    "/api/events/{event_id}",
    response_model=Union[EventWithOutCategories, Error],
)
def update_event(
    event_id: int,
    event: EventIn,
    repo: EventRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> Union[Error, EventWithOutCategories]:
    owner_id = account_data["id"]
    old = repo.get_event(event_id)
    if old.owner_id != owner_id:
        raise HTTPException(status_code=403, detail="User cannot update event")
    return repo.update(event_id, event, owner_id)
