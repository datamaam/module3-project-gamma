from fastapi import (
    Depends,
    APIRouter,
)
from authenticator import authenticator

from queries.attendees_queries import (
    AttendeesOut,
    AttendeesRepo,
    Attendee,
    EventsOut,
)

router = APIRouter(tags=["Attendees"])


@router.post("/api/events/{event_id}/attendees", response_model=Attendee)
def create_attendee(
    event_id: int,
    repo: AttendeesRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    user_id = account_data["id"]
    return repo.create(event_id, user_id)


@router.delete("/api/events/{event_id}/attendees", response_model=bool)
def delete_attendee(
    event_id: int,
    repo: AttendeesRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    user_id = account_data["id"]
    return repo.delete(event_id, user_id)


@router.get("/api/events/{event_id}/attendees", response_model=AttendeesOut)
def get_attendees_by_event(
    event_id: int,
    repo: AttendeesRepo = Depends(),
):
    return repo.get_all(event_id)


@router.get("/api/users/{user_id}/events", response_model=EventsOut)
def get_events_by_attendee(
    user_id: int,
    repo: AttendeesRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    user_id = account_data["id"]
    return repo.get_all_events(user_id)
