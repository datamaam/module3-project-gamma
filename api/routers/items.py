from fastapi import (
    Depends,
    HTTPException,
    Response,
    APIRouter,
)

from authenticator import authenticator
from typing import Union


from queries.user_queries import Error
from queries.item_queries import ItemIn, ItemOut, ItemsOut, ItemRepo


router = APIRouter(tags=["Items"])


# we could refactor so that its "api/events/{event_id}/items"
# so event_id is passed from url and not request body
@router.post("/api/items", response_model=ItemOut | Error)
def create_item(
    item: ItemIn,
    response: Response,
    repo: ItemRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    user_id = account_data["id"]
    return repo.create(item, user_id)


@router.get("/api/items/{item_id}", response_model=Union[ItemOut, Error])
def get_one_item(
    item_id: int,
    response: Response,
    repo: ItemRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> Union[ItemOut, Error]:
    item = repo.get_one(item_id)
    if item is None:
        raise HTTPException(status_code=404, detail="Item does not exist")
    return item


@router.get("/api/events/{event_id}/items", response_model=ItemsOut)
def get_items_by_event(event_id: int, repo: ItemRepo = Depends()):
    return {"items": repo.get_all_by_event(event_id)}


@router.put("/api/items/{item_id}", response_model=Union[ItemOut, Error])
def edit_item(
    item_id: int,
    item: ItemIn,
    response: Response,
    repo: ItemRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> Union[ItemOut, Error]:
    user_id = account_data["id"]
    old_item = repo.get_one(item_id)
    if old_item.user.id != user_id:
        raise HTTPException(status_code=403, detail="User cannot update item")
    return repo.update(item_id, item, user_id)


@router.delete("/api/items/{item_id}", response_model=bool)
def delete_item(
    item_id: int,
    repo: ItemRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    old_item = repo.get_one(item_id)
    user_id = account_data["id"]
    if old_item.user.id != user_id:
        raise HTTPException(status_code=403, detail="User cannot delete item")
    return repo.delete(item_id)
