from fastapi import (
    Depends,
    HTTPException,
    # status,
    Response,
    APIRouter,
    # Request,
)

# from jwtdown_fastapi.authentication import Token
# from authenticator import authenticator
from typing import Union, List

# from pydantic import BaseModel

from queries.categories_queries import CategoryRepo, Category
from queries.user_queries import Error

router = APIRouter(tags=["Categories"])


@router.get("/api/categories", response_model=Union[List[Category], Error])
def get_all_categories(repo: CategoryRepo = Depends()):
    return repo.get_all()


@router.get(
    "/api/categories/{category_id}", response_model=Union[Category, Error]
)
def get_one(
    category_id: int,
    response: Response,
    repo: CategoryRepo = Depends(),
) -> Union[Category, Error]:
    category = repo.get_one(category_id)
    if category is None:
        raise HTTPException(status_code=404, detail="Category not found")

    return category
