from fastapi.testclient import TestClient
from queries.categories_queries import CategoryRepo
from authenticator import authenticator
from typing import Optional
from main import app
from pydantic import BaseModel

client = TestClient(app)


class EmptyCategoryRepo:
    def get_one(self, category_id):
        return {
            "id": 1,
            "name": "Entree",
        }


class UserOut(BaseModel):
    id: int
    email: str
    first_name: str
    last_name: str
    phone_number: str
    bio: Optional[str]
    profile_image: Optional[str]


def dummy_account_data():
    return UserOut(
        id=7,
        email="pal@world.com",
        first_name="Pal",
        last_name="Berlington",
        phone_number="456-321-7985",
        bio="Time to rule the world with my pals",
        profile_image="https://shorturl.at/oqrsX",
    )


def test_get_one():
    app.dependency_overrides[CategoryRepo] = EmptyCategoryRepo
    app.dependency_overrides[authenticator.get_current_account_data] = (
        dummy_account_data
    )

    response = client.get("/api/categories/1")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "name": "Entree",
    }
