from fastapi.testclient import TestClient
from queries.attendees_queries import AttendeesRepo, AttendeesOut
from queries.user_queries import UserOut, BasicUserOut
from authenticator import authenticator
from main import app

client = TestClient(app)


class EmptyAttendeesRepo:
    def get_all(self, event_id):
        return AttendeesOut(
            attendees=[
                BasicUserOut(
                    id=14,
                    first_name="Amy",
                    last_name="Pirro",
                    profile_image=None,
                )
            ],
            event_id=event_id,
        )


def fake_get_current_account_data():
    return UserOut(
        id=14,
        email="amypirro@gmail.com",
        first_name="Amy",
        last_name="Pirro",
        phone_number="978-123-4567",
        bio="hi",
        profile_image=None,
    )


def test_get_attendees_by_event():
    app.dependency_overrides[authenticator.get_current_account_data] = (
        fake_get_current_account_data
    )
    app.dependency_overrides[AttendeesRepo] = EmptyAttendeesRepo

    response = client.get("/api/events/10/attendees")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == {
        "attendees": [
            {
                "id": 14,
                "first_name": "Amy",
                "last_name": "Pirro",
                "profile_image": None,
            }
        ],
        "event_id": 10,
    }
