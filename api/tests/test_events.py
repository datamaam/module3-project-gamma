from fastapi.testclient import TestClient
from queries.event_queries import EventRepo
from authenticator import authenticator
from typing import Optional
from pydantic import BaseModel
from main import app


client = TestClient(app)


class EmptyEventRepo:
    def get_event(self, event_id):
        return {
            "id": 3,
            "name": "string",
            "description": "string",
            "location": "string",
            "start_time": "2024-02-04T23:09:48.503000+00:00",
            "end_time": "2024-02-04T23:09:48.503000+00:00",
            "event_image": "string",
            "is_public": True,
            "max_attendees": 5,
            "owner_id": 1,
        }


class UserOut(BaseModel):
    id: int
    email: str
    first_name: str
    last_name: str
    phone_number: str
    bio: Optional[str]
    profile_image: Optional[str]


def dummy_account_data():
    return UserOut(
        id=1,
        email="email@email.com",
        first_name="where is",
        last_name="WALDO1",
        phone_number="555-458-5555",
        bio="WALDO1",
        profile_image="https://google.com",
    )


def test_get_one_event():
    app.dependency_overrides[EventRepo] = EmptyEventRepo
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = dummy_account_data

    response = client.get("/api/events/3")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == {
        "id": 3,
        "name": "string",
        "description": "string",
        "location": "string",
        "start_time": "2024-02-04T23:09:48.503000+00:00",
        "end_time": "2024-02-04T23:09:48.503000+00:00",
        "event_image": "string",
        "is_public": True,
        "max_attendees": 5,
        "owner_id": 1,
    }
