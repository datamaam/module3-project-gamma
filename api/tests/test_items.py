from fastapi.testclient import TestClient
from queries.item_queries import ItemRepo
from authenticator import authenticator
from typing import Optional
from pydantic import BaseModel
from main import app


client = TestClient(app)


class EmptyItemRepo:
    def get_one(self, item_id):
        return {
            "id": 24,
            "name": "Fruit Snacks",
            "description": "scooby dooby do",
            "quantity": 1,
            "item_image": "",
            "user": {
                "id": 12,
                "first_name": "Matt",
                "last_name": "Hanson",
                "profile_image": "https://shorturl.at/tKS48",
            },
            "event_id": 1,
            "category": {"id": 2, "name": "Sides"},
        }


class UserOut(BaseModel):
    id: int
    email: str
    first_name: str
    last_name: str
    phone_number: str
    bio: Optional[str]
    profile_image: Optional[str]


def dummy_account_data():
    return UserOut(
        id=1,
        email="email@email.com",
        first_name="Matt",
        last_name="Hanson",
        phone_number="555-458-5555",
        bio="lit",
        profile_image="https://shorturl.at/tKS48",
    )


def test_get_one_item():
    app.dependency_overrides[ItemRepo] = EmptyItemRepo
    app.dependency_overrides[authenticator.get_current_account_data] = (
        dummy_account_data
    )

    response = client.get("/api/items/24")
    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == {
        "id": 24,
        "name": "Fruit Snacks",
        "description": "scooby dooby do",
        "quantity": 1,
        "item_image": "",
        "user": {
            "id": 12,
            "first_name": "Matt",
            "last_name": "Hanson",
            "profile_image": "https://shorturl.at/tKS48",
        },
        "event_id": 1,
        "category": {"id": 2, "name": "Sides"},
    }
