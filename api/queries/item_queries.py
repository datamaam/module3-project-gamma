from pydantic import BaseModel
from typing import Union, Optional, List
from queries.pool import pool
from .user_queries import Error, UserRepository, BasicUserOut
from .categories_queries import Category, CategoryRepo
from fastapi import Depends, HTTPException


class ItemIn(BaseModel):
    name: str
    description: Optional[str]
    quantity: Optional[int]
    item_image: Optional[str]
    event_id: int
    category_id: Optional[int]


class ItemOut(BaseModel):
    id: int
    name: str
    description: Optional[str]
    quantity: Optional[int]
    item_image: Optional[str]
    user: Optional[BasicUserOut]
    event_id: int
    category: Optional[Category]


class ItemsOut(BaseModel):
    items: List[ItemOut]


class ItemRepo:
    def __init__(
        self,
        user_repo: UserRepository = Depends(UserRepository),
        cat_repo: CategoryRepo = Depends(CategoryRepo),
    ):
        self.user_repo = user_repo
        self.cat_repo = cat_repo

    def create(self, item: ItemIn, user_id: int) -> Union[ItemOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO items
                        (
                        name,
                        description,
                        quantity,
                        item_image,
                        user_id,
                        event_id,
                        category_id
                        )
                        VALUES
                        (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            item.name,
                            item.description,
                            item.quantity,
                            item.item_image,
                            user_id,
                            item.event_id,
                            item.category_id,
                        ],
                    )

                    id = result.fetchone()[0]

                    user_info = self.user_repo.get_one(user_id=user_id)
                    cat_info = self.cat_repo.get_one(
                        category_id=item.category_id
                    )

                    result_object = ItemOut(
                        id=id,
                        name=item.name,
                        description=item.description,
                        quantity=item.quantity,
                        item_image=item.item_image,
                        user=user_info,
                        event_id=item.event_id,
                        category=cat_info,
                    )
                    return result_object
        except Exception:
            raise HTTPException(status_code=500, detail="Something went wrong")

    def get_one(self, item_id: int) -> Union[ItemOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        *
                        FROM items
                        WHERE id = %s;
                        """,
                        [item_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    user_info = self.user_repo.get_one(user_id=record[5])
                    cat_info = self.cat_repo.get_one(category_id=record[7])
                    result_object = ItemOut(
                        id=record[0],
                        name=record[1],
                        description=record[2],
                        quantity=record[3],
                        item_image=record[4],
                        user=user_info,
                        event_id=record[6],
                        category=cat_info,
                    )
                    return result_object
        except Exception:
            raise HTTPException(status_code=500, detail="Something went wrong")

    def get_all_by_event(self, event_id: int) -> List[ItemOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            i.*,
                            c.name as category_name,
                            u.first_name,
                            u.last_name,
                            u.profile_image
                        FROM items as i
                        LEFT OUTER JOIN categories as c
                        ON i.category_id = c.id
                        LEFT OUTER JOIN users as u
                        ON i.user_id = u.id
                        WHERE event_id = %s;
                        """,
                        [event_id],
                    )
                    records = result.fetchall()
                    items_list = []
                    for record in records:
                        user_record = None
                        if record[5]:
                            user_record = BasicUserOut(
                                id=record[5],
                                first_name=record[9],
                                last_name=record[10],
                                profile_image=record[11],
                            )
                        category_record = None
                        if record[7]:
                            category_record = Category(
                                id=record[7], name=record[8]
                            )
                        one_item = ItemOut(
                            id=record[0],
                            name=record[1],
                            description=record[2],
                            quantity=record[3],
                            item_image=record[4],
                            user=user_record,
                            event_id=record[6],
                            category=category_record,
                        )
                        items_list.append(one_item)
                    return items_list
        except Exception:
            raise HTTPException(status_code=500, detail="Something went wrong")

    def update(
        self, item_id: int, item: ItemIn, user_id: int
    ) -> Union[ItemOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE items
                        SET
                        name = %s,
                        description = %s,
                        quantity = %s,
                        item_image = %s,
                        user_id = %s,
                        event_id = %s,
                        category_id = %s

                        WHERE id = %s
                        """,
                        [
                            item.name,
                            item.description,
                            item.quantity,
                            item.item_image,
                            user_id,
                            item.event_id,
                            item.category_id,
                            item_id,
                        ],
                    )
                    conn.commit()
                    updated_item = self.get_one(item_id)
                    return updated_item
        except Exception:
            raise HTTPException(
                status_code=403, detail="User cannot update item"
            )

    def delete(self, item_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM items
                        WHERE id = %s
                        """,
                        [item_id],
                    )
                    return True
        except Exception:
            raise HTTPException(
                status_code=403, detail="User cannot delete item"
            )
