from pydantic import BaseModel
from typing import Union, List
from queries.pool import pool
from .user_queries import Error
from fastapi import HTTPException


class Category(BaseModel):
    id: int
    name: str


class CategoryRepo:
    def get_all(self) -> Union[List[Category], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM categories
                        ORDER BY id;
                        """,
                    )
                    return [
                        self.record_to_category_out(record)
                        for record in result
                    ]
        except Exception as e:
            return Error(message=str(e))

    def get_one(self, category_id: int) -> Union[Category, Error, None]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, name
                        FROM categories
                        WHERE id = %s;
                        """,
                        [category_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return Category(id=record[0], name=record[1])
        except Exception:
            raise HTTPException(status_code=500, detail="Something went wrong")

    def record_to_category_out(self, record):
        return Category(
            id=record[0],
            name=record[1],
        )
