from pydantic import BaseModel
from typing import Optional, List
from queries.pool import pool
from fastapi import HTTPException
from queries.categories_queries import Category


class EventCategory(BaseModel):
    event_id: int
    category_id: int


class EventCategoriesOut(BaseModel):
    categories: Optional[List[Category]]
    event_id: int


class EventCategoryRepo:
    def create(self, event_id: int, category_id: int) -> EventCategory:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO event_categories (
                            event_id,
                            category_id
                        )
                        VALUES
                        (%s, %s);
                        """,
                        [
                            event_id,
                            category_id,
                        ],
                    )
                    return EventCategory(
                        event_id=event_id,
                        category_id=category_id,
                    )
        except Exception:
            raise HTTPException(
                status_code=500, detail="Somethings out of place"
            )

    def delete(self, event_id: int, category_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE from event_categories
                        WHERE event_id = %s AND category_id = %s;
                        """,
                        [event_id, category_id],
                    )
                    return True
        except Exception:
            raise HTTPException(
                status_code=400, detail="Could not delete category"
            )

    def get_all(self, event_id: int) -> EventCategoriesOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        c.name,
                        ec.*
                        FROM
                        events e
                        JOIN event_categories ec ON e.id = ec.event_id
                        JOIN categories c ON c.id = ec.category_id
                        WHERE e.id = %s;
                        """,
                        [event_id],
                    )
                    records = result.fetchall()
                    cats_list = []
                    for record in records:
                        one_cat = Category(
                            id=record[2],
                            name=record[0],
                        )
                        cats_list.append(one_cat)
                    return EventCategoriesOut(
                        categories=cats_list, event_id=event_id
                    )
        except Exception:
            raise HTTPException(status_code=500, detail="Something went wrong")
