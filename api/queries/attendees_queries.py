from pydantic import BaseModel
from typing import Optional, List
from queries.pool import pool
from .user_queries import BasicUserOut
from fastapi import HTTPException
from datetime import datetime


class Attendee(BaseModel):
    event_id: int
    user_id: int


class AttendeesOut(BaseModel):
    attendees: Optional[List[BasicUserOut]]
    event_id: int


class EventOut(BaseModel):
    id: int
    name: str
    start_time: datetime
    event_image: Optional[str]


class EventsOut(BaseModel):
    events: Optional[List[EventOut]]
    user_id: int


class AttendeesRepo:
    def create(self, event_id: int, user_id: int) -> Attendee:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO attendees (
                            event_id,
                            user_id
                        )
                        VALUES
                        (%s, %s);
                        """,
                        [
                            event_id,
                            user_id,
                        ],
                    )

                    return Attendee(
                        event_id=event_id,
                        user_id=user_id,
                    )
        except Exception:
            raise HTTPException(status_code=500, detail="Something went wrong")

    def delete(self, event_id: int, user_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE from attendees
                        WHERE event_id = %s AND user_ID = %s
                        """,
                        [event_id, user_id],
                    )
                    return True
        except Exception:
            raise HTTPException(
                status_code=400, detail="Could not delete user"
            )

    def get_all(self, event_id: int) -> AttendeesOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        u.first_name,
                        u.last_name,
                        u.profile_image,
                        a.*
                        FROM
                        events e
                        JOIN attendees a ON e.id = a.event_id
                        JOIN users u ON u.id = a.user_id
                        WHERE e.id = %s;
                        """,
                        [event_id],
                    )
                    records = result.fetchall()
                    attendees_list = []
                    for record in records:
                        one_attendee = BasicUserOut(
                            id=record[4],
                            first_name=record[0],
                            last_name=record[1],
                            profile_image=record[2],
                        )
                        attendees_list.append(one_attendee)
                    return AttendeesOut(
                        attendees=attendees_list, event_id=event_id
                    )
        except Exception:
            raise HTTPException(status_code=500, detail="Something went wrong")

    def get_all_events(self, user_id: int) -> EventsOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        e.name,
                        e.start_time,
                        e.event_image,
                        a.*
                        FROM
                        events e
                        JOIN attendees a ON e.id = a.event_id
                        JOIN users u ON u.id = a.user_id
                        WHERE u.id = %s;
                        """,
                        [user_id],
                    )
                    records = result.fetchall()
                    events_list = []
                    for record in records:
                        one_event = EventOut(
                            id=record[3],
                            name=record[0],
                            start_time=record[1],
                            event_image=record[2],
                        )
                        events_list.append(one_event)
                    return EventsOut(events=events_list, user_id=user_id)
        except Exception:
            raise HTTPException(status_code=500, detail="Something went wrong")
