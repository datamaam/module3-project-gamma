from pydantic import BaseModel
from typing import Union, List, Optional
from datetime import datetime
from queries.pool import pool
from .user_queries import Error
from fastapi import HTTPException

# from .categories_queries import Category


class DuplicateEventError(ValueError):
    pass


class EventIn(BaseModel):
    name: str
    description: Optional[str]
    location: Optional[str]
    start_time: datetime
    end_time: Optional[datetime]
    event_image: Optional[str]
    is_public: bool
    max_attendees: Optional[int]


class EventOut(BaseModel):
    id: int
    name: str
    owner_id: int
    description: Optional[str]
    location: Optional[str]
    start_time: datetime
    end_time: Optional[datetime]
    event_image: Optional[str]
    is_public: bool
    max_attendees: Optional[int]
    categories: List[int]


class EventWithOutCategories(BaseModel):
    id: int
    name: str
    description: Optional[str]
    location: Optional[str]
    start_time: datetime
    end_time: Optional[datetime]
    event_image: Optional[str]
    is_public: bool
    max_attendees: Optional[int]
    owner_id: int


class EventRepo:
    def delete(self, event_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM events
                        WHERE id = %s
                        """,
                        [event_id],
                    )
                    return True
        except Exception:
            return False

    def create(
        self, event: EventIn, owner_id: int
    ) -> Union[EventWithOutCategories, DuplicateEventError]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO events (
                            name,
                            owner_id,
                            description,
                            location,
                            start_time,
                            end_time,
                            event_image,
                            is_public,
                            max_attendees
                        )
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            event.name,
                            owner_id,
                            event.description,
                            event.location,
                            event.start_time,
                            event.end_time,
                            event.event_image,
                            event.is_public,
                            event.max_attendees,
                        ],
                    )
                    event_id = result.fetchone()[0]

                    conn.commit()
                    result_object = EventWithOutCategories(
                        id=event_id,
                        owner_id=owner_id,
                        name=event.name,
                        description=event.description,
                        location=event.location,
                        start_time=event.start_time,
                        end_time=event.end_time,
                        event_image=event.event_image,
                        is_public=event.is_public,
                        max_attendees=event.max_attendees,
                    )
                    return result_object
        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=403, detail="User cannot update event"
            )

    def get_event(self, event_id: int) -> Optional[EventWithOutCategories]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        id,
                        name,
                        description,
                        location,
                        start_time,
                        end_time,
                        event_image,
                        is_public,
                        max_attendees,
                        owner_id
                        FROM events
                        WHERE id = %s;
                        """,
                        [event_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_event_out(record)
        except Exception:
            return {"message": "Could not get one event"}

    def get_all_public(self) -> Union[List[EventWithOutCategories], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM events
                        WHERE is_public = true;
                        """,
                    )
                    print("WALDO", result)
                    return [
                        self.record_to_event_out(record) for record in result
                    ]
        except Exception as e:
            return Error(message=str(e))

    # def get_all_events(self) -> Union[List[EventWithOutCategories], Error]:
    #     try:
    #         with pool.connection() as conn:
    #             with conn.cursor() as db:
    #                 result = db.execute(
    #                     """
    #                     SELECT *
    #                     FROM events
    #                     """,
    #                 )
    #                 return [
    #                     self.record_to_event_out(record) for record in result
    #                 ]
    #     except Exception as e:
    #         return Error(message=str(e))

    def update(
        self, event_id: int, event: EventIn, owner_id: int
    ) -> Union[EventWithOutCategories, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE events
                        SET
                        name = %s,
                        description = %s,
                        location = %s,
                        start_time = %s,
                        end_time = %s,
                        event_image = %s,
                        is_public = %s,
                        max_attendees = %s,
                        owner_id = %s
                        WHERE id = %s;
                        """,
                        [
                            event.name,
                            event.description,
                            event.location,
                            event.start_time,
                            event.end_time,
                            event.event_image,
                            event.is_public,
                            event.max_attendees,
                            owner_id,
                            event_id,
                        ],
                    )
                    conn.commit()
                updated_event = self.get_event(event_id)
                return updated_event
        except Exception:
            raise HTTPException(
                status_code=403, detail="User cannot update event"
            )

    def record_to_event_out(self, record):
        return EventWithOutCategories(
            id=record[0],
            name=record[1],
            description=record[2],
            location=record[3],
            start_time=record[4],
            end_time=record[5],
            event_image=record[6],
            is_public=record[7],
            max_attendees=record[8],
            owner_id=record[9],
        )
