steps = [
    [
        # "Up" SQL statement
        """
        INSERT INTO categories (id, name)
        VALUES
            (1, 'Entrees'),
            (2, 'Sides'),
            (3, 'Desserts'),
            (4, 'Supplies'),
            (5, 'Misc.'),
            (6, 'Drinks'),
            (7, 'Games');
        """,
        # "Down" SQL statement
        """
        
        """
    ]
]
