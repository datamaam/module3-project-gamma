steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE events (
            id SERIAL PRIMARY KEY,
            name VARCHAR(150) NOT NULL CHECK (name <> ''),
            description TEXT,
            location TEXT,
            start_time TIMESTAMPTZ NOT NULL,
            end_time TIMESTAMPTZ,
            event_image TEXT,
            is_public BOOL NOT NULL,
            max_attendees SMALLINT,
            owner_id INT REFERENCES users (id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE events;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE categories (
            id SERIAL PRIMARY KEY,
            name VARCHAR(150) NOT NULL CHECK (name <> ''),
            user_id INT REFERENCES users(id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE categories;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE event_categories (
		    event_id INT REFERENCES events (id) ON DELETE CASCADE NOT NULL,
            category_id INT REFERENCES categories (id) ON DELETE CASCADE NOT NULL,
            PRIMARY KEY (event_id, category_id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE event_categories;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE items (
            id SERIAL PRIMARY KEY,
            name VARCHAR(150) NOT NULL,
            description TEXT,
            quantity SMALLINT DEFAULT 1,
            item_image TEXT,
            user_id INT REFERENCES users(id),
            event_id INT REFERENCES events(id) ON DELETE CASCADE NOT NULL,
            category_id INT REFERENCES categories(id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE items;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE attendees (
            event_id INT REFERENCES events(id) ON DELETE CASCADE NOT NULL,
            user_id INT REFERENCES users(id) ON DELETE CASCADE NOT NULL,
            PRIMARY KEY (event_id, user_id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE attendees;
        """
    ]
]
