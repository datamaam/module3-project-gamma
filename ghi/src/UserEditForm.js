import { useState } from "react";
import { HiInformationCircle } from "react-icons/hi";
import { Alert, Button, Textarea, Label, TextInput } from "flowbite-react";
import { useUserContext } from "./providers/UserProvider.js";

export default function UserEditForm({ setIsEditing }) {
  const { user, refreshUser } = useUserContext();
  const [username, setEmail] = useState(user.email);
  const [firstName, setFirstName] = useState(user.first_name);
  const [lastName, setLastName] = useState(user.last_name);
  const [phoneNumber, setPhoneNumber] = useState(user.phone_number);
  const [bio, setBio] = useState(user.bio);
  const [profileImage, setProfileImage] = useState(user.profile_image);
  const [invalidSignup, setInvalidSignup] = useState(false);


  const handleEmailChange = (e) => {
    const value = e.target.value;
    setEmail(value);
  };
  const handleFirstNameChange = (e) => {
    const value = e.target.value;
    setFirstName(value);
  };
  const handleLastNameChange = (e) => {
    const value = e.target.value;
    setLastName(value);
  };
  const handleNumberChange = (e) => {
    const value = e.target.value;
    setPhoneNumber(value);
  };
  const handleBioChange = (e) => {
    const value = e.target.value;
    setBio(value);
  };
  const handleImageChange = (e) => {
    const value = e.target.value;
    setProfileImage(value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const accountData = {
      email: username,
      first_name: firstName,
      last_name: lastName,
      phone_number: phoneNumber,
      bio: bio,
      profile_image: profileImage,
    };
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_HOST}/api/users`,
        {
          method: "PUT",
          body: JSON.stringify(accountData),
          headers: {
            "Content-Type": "application/json",
          },
          credentials: "include",
        }
      );
      if (response.ok) {
        await refreshUser();
        setIsEditing(false);
      } else {
        setInvalidSignup(true);
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-sm">
          <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
            Edit Your Profile
          </h2>
        </div>
        <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm space-y-6">
          {invalidSignup && (
            <Alert color="failure" icon={HiInformationCircle}>
              <span className="font-medium">
                Email or Phone number already exist
              </span>
            </Alert>
          )}
          <form
            onSubmit={handleSubmit}
            id="signup-form"
            className="flex max-w-md flex-col gap-4"
          >
            <div>
              <div className="mb-2 block">
                <Label htmlFor="email" value="Email" />
              </div>
              <TextInput
                onChange={handleEmailChange}
                value={username}
                id="email"
                type="email"
                autoComplete="email"
                placeholder="example@email.com"
                required
                shadow
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="first_name" value="First Name" />
              </div>
              <TextInput
                onChange={handleFirstNameChange}
                value={firstName}
                id="first_name"
                type="text"
                autoComplete="first_name"
                required
                shadow
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="last_name" value="Last Name" />
              </div>
              <TextInput
                onChange={handleLastNameChange}
                value={lastName}
                id="last_name"
                autoComplete="last_name"
                type="text"
                required
                shadow
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="phone_number" value="Phone Number" />
              </div>
              <TextInput
                onChange={handleNumberChange}
                value={phoneNumber}
                id="phone_number"
                autoComplete="phone_number"
                type="text"
                required
                shadow
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="bio" value="Bio" />
              </div>
              <Textarea
                onChange={handleBioChange}
                value={bio}
                id="bio"
                autoComplete="bio"
                type="text"
                shadow
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="profile_image" value="Profile Image" />
              </div>
              <TextInput
                onChange={handleImageChange}
                value={profileImage}
                id="profile_image"
                autoComplete="profile_image"
                type="text"
                shadow
              />
            </div>
            <Button className="mt-2" gradientDuoTone="tealToLime" type="submit">
              Update
            </Button>
          </form>
          <Button
            onClick={(e) => setIsEditing(false)}
            fullSized
            outline color="light"
            size="xs"
          >
            Cancel
          </Button>
        </div>
      </div>
    </>
  );
}
