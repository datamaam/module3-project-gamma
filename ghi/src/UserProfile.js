import { useState } from "react";
import { Card, Avatar, Button } from "flowbite-react";
import { useUserContext } from "./providers/UserProvider.js";
import UserEditForm from "./UserEditForm.js";

function UserProfile() {
  const { user } = useUserContext();
  const [isEditing, setIsEditing] = useState(false)



  return (
    <>
      {isEditing && <UserEditForm setIsEditing={setIsEditing} />}

      {!isEditing && user && (
        <div className="container mx-auto my-10">
          <h1 className="text-3xl text-center mb-5 font-semibold">
            Your Profile
          </h1>
          <Card className="max-w-md mx-auto">
            <div className="flex flex-col items-center pb-5">
              <Avatar
                alt="user profile image"
                img={user.profile_image}
                size="xl"
                rounded
                className="mb-3 rounded-full shadow-lg"
              />
              <h5 className="mb-1 text-xl font-medium text-gray-900 dark:text-white">
                {user.first_name} {user.last_name}
              </h5>
              <span className="text-sm text-gray-500 dark:text-gray-400">
                {user.email}
              </span>
            </div>

            <ul className="divide-y divide-gray-200 dark:divide-gray-700">
              <li>
                <div className="flex items-baseline space-x-6 mb-3">
                  <div className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    Bio
                  </div>
                  <div className="min-w-0 flex-1">
                    <p className="text-sm font-normal text-gray-900 dark:text-white">
                      {user.bio}
                    </p>
                  </div>
                </div>
              </li>
              <li>
                <div className="flex items-baseline space-x-6 mt-3">
                  <div className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    Tel
                  </div>
                  <div className="min-w-0 flex-1">
                    <p className="text-sm font-normal text-gray-900 dark:text-white">
                      {user.phone_number}
                    </p>
                  </div>
                </div>
              </li>
            </ul>
            <Button
              onClick={() => setIsEditing(true)}
              gradientDuoTone="tealToLime"
              className="mt-5 mb-2"
            >
              Edit
            </Button>
          </Card>
        </div>
      )}
    </>
  );
}

export default UserProfile;
