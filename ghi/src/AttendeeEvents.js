import React, { useEffect, useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { Card } from "flowbite-react";
import { Link } from "react-router-dom";
import defaultbg from "./assets/defaultbg.jpg";
import { useUserContext } from "./providers/UserProvider.js";

function AttendeeEvents() {
    const [events, setEvents] = useState([]);
    const { token } = useToken();
    const { user } = useUserContext();

    const eventData = async (e) => {
        const url = `${process.env.REACT_APP_API_HOST}/api/users/${user.id}/events`;

        const fetchConfig = {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
        credentials: "include",
        };
        try {
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const data = await response.json();

            const requests = [];
            for (let event of data.events) {
            const eventUrl = `${process.env.REACT_APP_API_HOST}/api/events/${event.id}`;
            requests.push(fetch(eventUrl, fetchConfig));
            }
            const responses = await Promise.all(requests);
            const eventsData = [];
            for (const eventResponse of responses) {
            if (eventResponse.ok) {
                const eventDetails = await eventResponse.json();
                eventsData.push(eventDetails);
            } else {
                console.error(eventResponse);
            }
            }
            setEvents(eventsData);
        }
        } catch (e) {
        console.error(e);
        }
    };
    useEffect(() => {
        if (user) {
        eventData();
        }
        // eslint-disable-next-line
    }, [user, token]);

    return (
        <>
        <div className="flex justify-center mb-4 sm:mx-auto sm:w-full sm:max-w-sm">
            <h1 className="mt-10 text-center text-3xl font-bold leading-9 tracking-tight text-gray-900">
            Upcoming events
            </h1>
        </div>
        <div className="flex flex-wrap">
            {events
            .filter((event) => new Date(event.start_time) >= Date.now())
            .map((event) => {
                return (
                <Card
                    key={event.id}
                    className="card w-full my-4 mx-6 sm:w-auto md:w-1/3 lg:w-1/4 xl:w-1/5"
                    imgSrc={event.event_image || defaultbg}
                    horizontal
                >
                    <Link
                    to={`/event/${event.id}`}
                    className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white"
                    >
                    {event.name}
                    </Link>
                    <p className="font-normal text-gray-700 dark:text-gray-400">
                    {new Date(event.start_time).toLocaleDateString()}
                    {", "}
                    {new Date(event.start_time).toLocaleTimeString("en-US", {
                        hour: "numeric",
                        minute: "numeric",
                    })}
                    </p>
                </Card>
                );
            })}
            {events.filter((event) => new Date(event.start_time) >= Date.now())
            .length === 0 && (
            <div className="flex justify-center mb-4 sm:mx-auto sm:w-full sm:max-w-sm">
                <h1 className="mt-5 text-center text-xl font-bold leading-9 tracking-tight text-gray-900">
                You have no upcoming events. 😕
                </h1>
            </div>
            )}
        </div>
        <div className="flex justify-center mt-20 mb-3 sm:mx-auto sm:w-full sm:max-w-sm">
            <h1 className="mt-10 text-center text-3xl font-bold leading-9 tracking-tight text-gray-900">
            Past events
            </h1>
        </div>
        <div className="flex flex-wrap">
            {events
            .filter((event) => new Date(event.start_time) <= Date.now())
            .map((event) => {
                return (
                <Card
                    key={event.id}
                    className="card w-full my-3 mx-6 sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/5"
                    imgSrc={event.event_image || defaultbg}
                    horizontal
                >
                    <Link
                    to={`/event/${event.id}`}
                    className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white"
                    >
                    {event.name}
                    </Link>
                    <p className="font-normal text-gray-700 dark:text-gray-400">
                    {new Date(event.start_time).toLocaleDateString()}
                    {", "}
                    {new Date(event.start_time).toLocaleTimeString("en-US", {
                        hour: "numeric",
                        minute: "numeric",
                    })}
                    </p>
                </Card>
                );
            })}
            {events.filter((event) => new Date(event.start_time) <= Date.now())
            .length === 0 && (
            <div className="flex justify-center mb-4 sm:mx-auto sm:w-full sm:max-w-sm">
                <h1 className="mt-5 text-center text-xl font-bold leading-9 tracking-tight text-gray-900">
                You have no past events. 😕
                </h1>
            </div>
            )}
        </div>
        </>
    );
}

export default AttendeeEvents;
