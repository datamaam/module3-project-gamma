import useToken from "@galvanize-inc/jwtdown-for-react";
import { jwtDecode } from "jwt-decode";
import {
  createContext,
  useContext,
  useEffect,
  useMemo,
  useState,
  useCallback,
} from "react";

// Example:
// App.js
// --------------------------------------
// const App = () => {
//
//   return (
//     <AuthProvider baseUrl="...">
//       <UserProvider>
//         <Routing />
//       </UserProvider>
//     </AuthProvider>
//   )
// }
//
// MyComponent.js
// --------------------------------------
// const MyComponent = () => {
//   const { user } = useUserContext()
//
//   if (!user) {
//     return <p>No user logged in</p>
//   }
//
//   return (
//     <div>
//       <p>{user.id}</p>
//     </div>
//   )
// }

const userContext = createContext(null);

export const useUserContext = () => {
  const context = useContext(userContext);
  if (!context) {
    throw new Error("useUserContext must be used within a UserProvider");
  }
  return context;
};

// function to get one user
const getOneUser = async (user_id) => {
  const response = await fetch(
    `${process.env.REACT_APP_API_HOST}/api/users/${user_id}`,
    {
      headers: {
        "Content-Type": "application/json",
        // Authorization: `Bearer ${token}`,
      },
      credentials: "include",
    }
  );
  if (!response.ok) {
    return null;
  }
  return response.json();
};

export const UserProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  // Loading state
  const { token } = useToken();

  const refreshUser = useCallback(async () => {
    const res = await getOneUser(user.id);
    if (res) {
      setUser(res);
    }
  }, [user, setUser]);

  useEffect(() => {
    async function fetchNewUser(id) {
      const res = await getOneUser(id);
      if (res) {
        setUser(res);
      }
    }
    if (token) {
      const decoded = jwtDecode(token);
      fetchNewUser(decoded.account.id);
    } else {
      setUser(null);
    }
  }, [token, setUser]);

  const value = useMemo(() => {
    return {
      user,
      refreshUser,
    };
    // eslint-disable-next-line
  }, [user]);

  return <userContext.Provider value={value}>{children}</userContext.Provider>;
};
