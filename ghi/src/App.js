// import { useEffect, useState } from "react";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Nav from "./Nav";
import SignupForm from "./SignupForm.js";
import MainPage from "./MainPage.js";
import LoginForm from "./LoginForm.js";
import ItemForm from "./ItemForm.js";
import PrivateRoutes from "./components/PrivateRoutes.js";
import EventsDetails from "./EventDetails.js";
import EventForm from "./EventForm.js";
import EventEditForm from "./EditEvent.js";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import { UserProvider } from "./providers/UserProvider.js";
import PublicEvent from "./PublicEvent.js";
import AttendeeEvents from "./AttendeeEvents.js";
import UserProfile from "./UserProfile.js";
import NotFound from "./NotFoundPage.js";
// import "EventsDetailsData.js"

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");
  // const [launchInfo, setLaunchInfo] = useState([]);
  // const [error, setError] = useState(null);

  // useEffect(() => {
  //   async function getData() {
  //     let url = `${process.env.REACT_APP_API_HOST}/api/launch-details`;
  //     console.log("fastapi url: ", url);
  //     let response = await fetch(url);
  //     console.log("------- hello? -------");
  //     let data = await response.json();

  //     if (response.ok) {
  //       console.log("got launch data!");
  //       setLaunchInfo(data.launch_details);
  //     } else {
  //       console.log("drat! something happened");
  //       setError(data.message);
  //     }
  //   }
  //   getData();
  // }, []);

  return (
    <AuthProvider baseUrl={process.env.REACT_APP_API_HOST}>
      <UserProvider>
        <BrowserRouter basename={basename}>
          <Nav />
          <Routes>
            <Route path="*" element={<NotFound />} />
            <Route path="/" element={<MainPage />} />
            <Route path="signup" element={<SignupForm />} />
            <Route path="login" element={<LoginForm />} />
            <Route path="event/:id/item" element={<ItemForm />} />
            <Route path="events/public" element={<PublicEvent />} />
            <Route path="event/:id" element={<EventsDetails />} />
            <Route path="event/:id/update" element={<EventEditForm />} />

            <Route element={<PrivateRoutes />}>
              {/* Protected routes here */}
              <Route path="my-events" element={<AttendeeEvents />} />
              <Route path="events/create" element={<EventForm />} />
              <Route path="my-profile" element={<UserProfile />} />
            </Route>
          </Routes>
        </BrowserRouter>
      </UserProvider>
    </AuthProvider>
  );
}

export default App;
