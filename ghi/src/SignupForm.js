import { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
import { Button, Textarea, Label, TextInput } from "flowbite-react";
import { HiInformationCircle } from "react-icons/hi";
import { Alert } from "flowbite-react";
import potlucky from "./assets/potlucky.svg";


function SignupForm() {
    const [username, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [bio, setBio] = useState("");
    const [profileImage, setProfileImage] = useState("");
    const [invalidSignup, setInvalidSignup] = useState(false);
    const { login } = useToken();
    const navigate = useNavigate();

    const handleSubmit = async (e) => {
    e.preventDefault();
    const accountData = {
        email: username,
        password: password,
        first_name: firstName,
        last_name: lastName,
        phone_number: phoneNumber,
        bio: bio,
        profile_image: profileImage,
    }
    try {
        const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/users`, {
            method: "POST",
            body: JSON.stringify(accountData),
            headers: {
                "Content-Type": "application/json",
            },
            })
        if (response.ok) {
            await login(accountData.email, accountData.password)
            e.target.reset();
            navigate("/")
        } else {
            setInvalidSignup(true)
        }
    } catch (error) {
        console.error(error)
    }


};
const handleEmailChange = (e) => {
const value = e.target.value;
setEmail(value);
};
const handlePasswordChange = (e) => {
const value = e.target.value;
setPassword(value);
};
const handleFirstNameChange = (e) => {
const value = e.target.value;
setFirstName(value);
};
const handleLastNameChange = (e) => {
const value = e.target.value;
setLastName(value);
};
const handleNumberChange = (e) => {
const value = e.target.value;
setPhoneNumber(value);
};
const handleBioChange = (e) => {
const value = e.target.value;
setBio(value);
};
const handleImageChange = (e) => {
const value = e.target.value;
setProfileImage(value);
};

return (
<>
    <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-sm">
            <img
                className="mx-auto h-10 w-auto"
                src={potlucky}
                alt="Your Company"
            />
            <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
                Sign up!
            </h2>
        </div>
    <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm space-y-6">
        {invalidSignup ? (
            <Alert color="failure" icon={HiInformationCircle}>
                <span className="font-medium">Email or Phone number already exist</span>
            </Alert>
        ) : null}
        <form
        onSubmit={handleSubmit}
        id="signup-form"
        className="flex max-w-md flex-col gap-4"
        >
        <div>
            <div className="mb-2 block">
            <Label htmlFor="email" value="Email" />
            </div>
            <TextInput
            onChange={handleEmailChange}
            value={username}
            id="email"
            type="email"
            autoComplete="email"
            placeholder="example@email.com"
            required
            shadow
            />
        </div>
        <div>
            <div className="mb-2 block">
            <Label htmlFor="password" value="Password" />
            </div>
            <TextInput
            onChange={handlePasswordChange}
            value={password}
            id="password"
            type="password"
            autoComplete="current-password"
            required
            shadow
            />
        </div>
        <div>
            <div className="mb-2 block">
            <Label htmlFor="first_name" value="First Name" />
            </div>
            <TextInput
            onChange={handleFirstNameChange}
            value={firstName}
            id="first_name"
            type="text"
            autoComplete="first_name"
            required
            shadow
            />
        </div>
        <div>
            <div className="mb-2 block">
            <Label htmlFor="last_name" value="Last Name" />
            </div>
            <TextInput
            onChange={handleLastNameChange}
            value={lastName}
            id="last_name"
            autoComplete="last_name"
            type="text"
            required
            shadow
            />
        </div>
        <div>
            <div className="mb-2 block">
            <Label htmlFor="phone_number" value="Phone Number" />
            </div>
            <TextInput
            onChange={handleNumberChange}
            value={phoneNumber}
            id="phone_number"
            autoComplete="phone_number"
            type="text"
            required
            shadow
            />
        </div>
        <div>
            <div className="mb-2 block">
            <Label htmlFor="bio" value="Bio" />
            </div>
            <Textarea
            onChange={handleBioChange}
            value={bio}
            id="bio"
            autoComplete="bio"
            type="text"
            shadow
            />
        </div>
        <div>
            <div className="mb-2 block">
            <Label htmlFor="profile_image" value="Profile Image" />
            </div>
            <TextInput
            onChange={handleImageChange}
            value={profileImage}
            id="profile_image"
            autoComplete="profile_image"
            type="text"
            shadow
            />
        </div>
        <Button outline gradientDuoTone="tealToLime" type="submit">
            Sign Up!
        </Button>
        </form>
        </div>
    </div>
</>
);
}

export default SignupForm;
