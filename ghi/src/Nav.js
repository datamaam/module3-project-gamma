import { NavLink } from "react-router-dom";
import { Navbar, Button, Avatar, Dropdown } from "flowbite-react";
import potlucky from "./assets/potlucky.svg";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useUserContext } from "./providers/UserProvider.js";

function Nav() {
  const { logout } = useToken();
  const { user } = useUserContext();

  return (
    <Navbar fluid className="bg-yellow-300">
      <Navbar.Brand to="/" as={NavLink}>
        <img
          src={potlucky}
          className="h-6"
          alt="Potlucky logo"
        />
      </Navbar.Brand>
      <div className="flex md:order-2">
        {user ? (
          <>
            <Dropdown
              arrowIcon={false}
              inline
              label={
                <Avatar alt="User settings" img={user.profile_image} rounded />
              }
            >
              <Dropdown.Header>
                <span className="block text-sm">
                  {user.first_name} {user.last_name}
                </span>
              </Dropdown.Header>
              <Dropdown.Item to="/my-profile" as={NavLink}>
                My profile
              </Dropdown.Item>
              <Dropdown.Item onClick={() => logout()}>Sign out</Dropdown.Item>
            </Dropdown>
          </>
        ) : (
          <>
            <Button to="/login" as={NavLink} color="dark">
              Log in
            </Button>
          </>
        )}

        <Navbar.Toggle />
      </div>
      <Navbar.Collapse>
        <Navbar.Link to="/events/public" as={NavLink}>
          Public Events
        </Navbar.Link>

        {user && (
          <>
            <Navbar.Link to="/events/create" as={NavLink}>
              Create an Event
            </Navbar.Link>
            <Navbar.Link to="/my-events" as={NavLink}>
              My events
            </Navbar.Link>
          </>
        )}
      </Navbar.Collapse>
    </Navbar>
  );
}

export default Nav;
