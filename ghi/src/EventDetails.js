import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useUserContext } from "./providers/UserProvider";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { Button} from "flowbite-react";
import MustBeSignedIn from "./MustBeSignedIn";
import NotFound from "./NotFoundPage";
import AttendeeList from "./AttendeeList.js";






const EventsDetails = () =>{
  const navigate = useNavigate();
  const { id } = useParams();
  const { user } = useUserContext();
  const { token } = useToken();
  const [Attendees_List, setAttendeesList] = useState([]);
  const [CatName, setCatNameList] = useState([]);
  const [Event_Items, setItemList] = useState();
  const [Event_Response, setEventList] = useState({
    name: "",
    description: "",
    location: "",
    start_time: "",
    end_time: "",
    event_image: "",
    max_attendees: 0,
    owner_id: "",
  });


  const header = { headers: { Authorization: `Bearer ${token}` } };
  // get catagory data________________________________________________________________________
  // get Attendees list
   async function GetAttendeeData() {
     try {
       const Attendee_url = `${process.env.REACT_APP_API_HOST}/api/events/${id}/attendees`;
       const Attendee_response_url = await fetch(Attendee_url, header);

       if (Attendee_response_url.ok) {
         const Attendee_response = await Attendee_response_url.json();
         setAttendeesList(Attendee_response.attendees);
       }
     } catch (e) {
       console.error(e);
     }
   }
  //  get Catagory data
  async function GetCatData() {
    try {
      const CatName_url = `${process.env.REACT_APP_API_HOST}/api/categories`;
      const CatName_response_url = await fetch(CatName_url, header);

      if (CatName_response_url.ok) {
        const CatName_response = await CatName_response_url.json();
        setCatNameList(CatName_response);
      }
    } catch (e) {
      console.error(e);
    }
  }
  // get item data
  async function GetItemData() {
    try {
      const items_url = `${process.env.REACT_APP_API_HOST}/api/events/${id}/items`;
      const items_response_url = await fetch(items_url, header);
      if (items_response_url.ok) {
        const items_response = await items_response_url.json();
        const items = items_response.items;
        // sort logic if more cats are added add 1 more list vv here vv
        const Item_sorted = [[], [], [], [], [], [], []];
        for (let i = 0; i < items.length; i++) {
          let catId = items[i]["category"]["id"] - 1;
          Item_sorted[catId].push(items[i]);
        }
        setItemList(Item_sorted);
      }
    } catch (e) {
      console.error(e);
    }
  }

  async function GetEventData() {

    try {
      const event_url = `${process.env.REACT_APP_API_HOST}/api/events/${id}`;
      const Event_Response_url = await fetch(event_url, header);
      if (Event_Response_url.ok) {
        const Event_Response = await Event_Response_url.json();
        let start_time_parsed = new Date(Event_Response.start_time);
        let end_time_parsed = new Date(Event_Response.end_time);
        let options = {
          day: "numeric",
          month: "numeric",
          year: "numeric",
          hour: "numeric",
          minute: "numeric",
          hour12: true, // Use 12-hour clock
        };
        if (Event_Response.event_image < 2) {
          setEventList({
            name: Event_Response.name,
            description: Event_Response.description,
            location: Event_Response.location,
            start_time: start_time_parsed.toLocaleString(undefined, options),
            end_time: end_time_parsed.toLocaleString(undefined, options),
            event_image:
              "https://images.pexels.com/photos/11256830/pexels-photo-11256830.jpeg?auto=compress&cs=tinysrgb&w=600",
            max_attendees: Event_Response.max_attendees,
            owner_id: Event_Response.owner_id,
          });
        } else {
          setEventList({
            name: Event_Response.name,
            description: Event_Response.description,
            location: Event_Response.location,
            start_time: start_time_parsed.toLocaleString(undefined, options),
            end_time: end_time_parsed.toLocaleString(undefined, options),
            event_image: Event_Response.event_image,
            max_attendees: Event_Response.max_attendees,
            owner_id: Event_Response.owner_id,
          });
        }

      }
    } catch (e) {
      console.error(e);
    }
  }
  const handleUpdate = () => {
    navigate(`/event/${id}/update`);
  };

  useEffect(() => {
    if (token) {
      GetAttendeeData();
      GetEventData();
      GetItemData();
      GetCatData();
    }
  // eslint-disable-next-line
  }, [token]);
  // ____________________________________________________________________________________________
// buttons
// del item
async function handleDelItem(itemId) {
  try {
    const item_del_url = `${process.env.REACT_APP_API_HOST}/api/items/${itemId}`;
    const fetchConfig = {
      method: "DELETE",
      body: JSON.stringify(""),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      credentials: "include",
    };
    const attendee_del_response = await fetch(item_del_url, fetchConfig);

    if (attendee_del_response.ok) {
       GetItemData();
    }
  } catch (e) {
    console.error(e);
  }
}

  //   link to RSVP
    async function handleUnRSVP() {
      try {
        const attendee_post_url = `${process.env.REACT_APP_API_HOST}/api/events/${id}/attendees`;
        const fetchConfig = {
          method: "DELETE",
          body: JSON.stringify(""),
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          credentials: "include",
        };
        const attendee_post_response = await fetch(
          attendee_post_url,
          fetchConfig
        );

        if (attendee_post_response.ok) {
          GetAttendeeData();
        }
      } catch (e) {
        console.error(e);
      }
    }
    //
    async function handleRSVP (){
      try{const attendee_post_url = `${process.env.REACT_APP_API_HOST}/api/events/${id}/attendees`;
       const fetchConfig = {
         method: "post",
         body: JSON.stringify(''),
         headers: {
           "Content-Type": "application/json",
           Authorization: `Bearer ${token}`,
         },
         credentials: "include",
       };
      const attendee_post_response = await fetch(
        attendee_post_url,
        fetchConfig
      );

       if (attendee_post_response.ok) {
         GetAttendeeData();
        };
      }catch (e) {
      console.error(e);
    }
    }
    function itemOwner(item) {
      if (user.id === item.user.id) {
        return true;
      }
      return false;
    }
    function isAttendee (){
        for (let i=0; i<Attendees_List.length; i++){
          if( user.id === Attendees_List[i].id){
            return true
          }
        }
        return false
    }
  //  mat fixed this part link to add new item
  const handleAddItem = () => {
    navigate(`/event/${id}/item`);
  };
  //  end mat insert
  // const handleSignup = () => {
  //   navigate(`/signup`);
  // };
//  end buttons
    if (token == null){
      return (
        <MustBeSignedIn />
      );}
    else if (Event_Response.name === "") {
      return(
        <NotFound />
      )
    }
    else if (Event_Items) {
      return (
        <>
          <div className="py-8 px-4 mx-auto max-w-screen-xl lg:py-16 lg:px-6">
            <div className="grid grid-cols-2 md:grid-cols-3 gap-4  center no-repeat bg-[url{Event_Response.event_image}] bg-blend-multiply">
              <div className="col-span-1 w-full max-w-md p-4 bg-white border border-gray-200 rounded-lg shadow sm:p-8 dark:bg-gray-800 dark:border-gray-700">
                <h2 className="mb-3 text-4xl font-extrabold leading-none tracking-tight text-gray-900  dark:text-white">
                  {Event_Response.name}
                </h2>
                <p>Starts: {Event_Response.start_time}</p>
                <p>Ends: {Event_Response.end_time}</p>
                <p>address: {Event_Response.location}</p>
                {user.id === Event_Response.owner_id && (
                  <Button
                    onClick={handleUpdate}
                    gradientDuoTone="pinkToOrange"
                    className="w-full text-white  focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Update event
                  </Button>
                )}
              </div>
              {Event_Response.event_image ? (
                <div className="col-span-2 w-full max-w-md p-4 bg-white border border-gray-200 rounded-lg shadow sm:p-8 dark:bg-gray-800 dark:border-gray-700">
                  <img
                    alt="event header visual or loading didnt go great"
                    src={Event_Response.event_image}
                  />
                </div>
              ) : (
                <></>
              )}
            </div>

            {/* new container */}
            <div
              id="card-container"
              className="grid grid-cols-2 md:grid-cols-3 gap-4"
            >
              {/* attendee card */}
              <div
                id="card"
                className="w-full max-w-md p-4 bg-white border border-gray-200 rounded-lg shadow sm:p-8 dark:bg-gray-800 dark:border-gray-700"
              >
                <div className="flow-root">
                  <AttendeeList attendees={Attendees_List} />
                </div>
                {/* RSVP button */}
                {isAttendee() ? (
                  <>
                    <span className="p-1">
                      <Button
                        onClick={handleUnRSVP}
                        gradientDuoTone="pinkToOrange"
                        className="w-full text-white  focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                      >
                        Can't go anymore?
                      </Button>
                    </span>
                    <span>
                      <Button
                        onClick={handleAddItem}
                        gradientDuoTone="pinkToOrange"
                        className="w-full text-white  focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                      >
                        bring an item?
                      </Button>
                    </span>
                  </>
                ) : (
                  <>
                    <Button
                      onClick={handleRSVP}
                      gradientDuoTone="pinkToOrange"
                      className="w-full text-white  focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                    >
                      I'm Going
                    </Button>
                  </>
                )}
              </div>
              {/*  */}
              {/* card items start */}
              {CatName.map((cat) => {
                return (
                  <div
                    id="card"
                    className="w-full max-w-md p-4 bg-white border border-gray-200 rounded-lg shadow sm:p-8 dark:bg-gray-800 dark:border-gray-700"
                    key={cat["id"]}
                  >
                    <div className="flex items-center justify-between mb-4">
                      {cat["name"]}
                      <span> Quantity </span>
                    </div>
                    <div className="flow-root">
                      <ul className="divide-y divide-gray-200 dark:divide-gray-700">
                        {Event_Items[cat["id"] - 1].map((item) => {
                          return (
                            <li key={item.id} className="py-4 sm:py-4">
                              <div className="flex items-center">
                                <div className="flex-shrink-0">
                                  <img
                                    className="w-8 h-8 rounded-full"
                                    src={item.item_image}
                                    alt="item"
                                  />
                                </div>
                                <div className="flex-1 min-w-0 ms-4">
                                  <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                                    {item.name}
                                  </p>
                                  <p className="text-sm text-gray-500 truncate dark:text-gray-400">
                                    {item.description}
                                  </p>

                                  {itemOwner(item) ? (
                                    <div>
                                      <Button
                                        onClick={(event) =>
                                          handleDelItem(item.id)
                                        }
                                        type="button"
                                        className=" text-white bg-gradient-to-r from-red-400 via-red-500 to-red-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800"
                                      >
                                        remove item
                                      </Button>
                                    </div>
                                  ) : (
                                    <>
                                      <p>
                                        {" "}
                                        {item.user.first_name}{" "}
                                        {item.user.last_name}
                                      </p>
                                    </>
                                  )}
                                </div>
                                <div className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                                  {item.quantity}
                                </div>
                              </div>
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  </div>
                );
              })}
            </div>

            {/* button's */}
          </div>
        </>
      );
    }
}




  export default EventsDetails;
