import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { Label, Button, TextInput } from "flowbite-react";

export default function EventEditForm() {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [location, setLocation] = useState("");
  const [startTime, setStartTime] = useState("");
  const [endTime, setEndTime] = useState("");
  const [eventImage, setEventImage] = useState("");
  const [isPublic, setIsPublic] = useState(false);
  const [maxAttendees, setMaxAttendees] = useState("");
  const navigate = useNavigate();
  const { id } = useParams();
  const { token } = useToken();

  useEffect(() => {
    const fetchEvent = async () => {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_HOST}/api/events/${id}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        if (response.ok) {
          const eventData = await response.json();
          setName(eventData.name);
          setDescription(eventData.description);
          setLocation(eventData.location);
          setStartTime(new Date(eventData.start_time).toISOString().slice(0, -1));
          setEndTime(new Date(eventData.end_time).toISOString().slice(0, -1));
          setEventImage(eventData.event_image);
          setIsPublic(eventData.is_public);
          setMaxAttendees(eventData.max_attendees);
        }
      } catch (e) {
        console.error(e);
      }
    };

    if (id && token) {
      fetchEvent();
    }
  }, [id, token]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const accountData = {
      name: name,
      description: description,
      location: location,
      start_time: new Date(startTime).toISOString(),
      end_time: new Date(endTime).toISOString(),
      event_image: eventImage,
      is_public: isPublic,
      max_attendees: maxAttendees,
    };

    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_HOST}/api/events/${id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify(accountData),
        }
      );

      if (response.ok) {
        navigate(`/event/${id}`);
      } else {
        const errorData = await response.json();
        console.error("Failed to update event:", errorData.error);
      }
    } catch (e) {
      console.e("Error updating event:", e);
    }
  };

  return (
    <>
      <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-sm">
          <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
            Edit Your Event
          </h2>
        </div>
        <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm space-y-6">
          <form
            onSubmit={handleSubmit}
            id="event-form"
            className="flex max-w-md flex-col gap-4"
          >
            <div>
              <div className="mb-2 block">
                <Label htmlFor="name" value="Name" />
              </div>
              <TextInput
                onChange={(e) => setName(e.target.value)}
                value={name}
                id="name"
                type="text"
                autoComplete="on"
                placeholder="example@email.com"
                required
                shadow
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="description" value="Description" />
              </div>
              <TextInput
                onChange={(e) => setDescription(e.target.value)}
                value={description}
                id="description"
                type="text"
                autoComplete="on"
                required
                shadow
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="location" value="Location" />
              </div>
              <TextInput
                onChange={(e) => setLocation(e.target.value)}
                value={location}
                id="location"
                autoComplete="location"
                type="text"
                required
                shadow
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="startTime" value="Start Time" />
              </div>
              <TextInput
                onChange={(e) => setStartTime(e.target.value)}
                value={startTime}
                id="startTime"
                autoComplete="on"
                type="datetime-local"
                required
                shadow
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="endTime" value="End Time" />
              </div>
              <TextInput
                onChange={(e) => setEndTime(e.target.value)}
                value={endTime}
                id="endTime"
                autoComplete="endTime"
                type="datetime-local"
                shadow
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="eventImage" value="Event Image" />
              </div>
              <TextInput
                onChange={(e) => setEventImage(e.target.value)}
                value={eventImage}
                id="eventImage"
                autoComplete="on"
                type="url"
                shadow
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="isPublic" value="Is Public" />
              </div>
              <TextInput
                onChange={(e) => setIsPublic(e.target.checked)}
                value={isPublic}
                id="isPublic"
                autoComplete="on"
                type="checkbox"
                shadow
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="maxAttendees" value="Max Attendee" />
              </div>
              <TextInput
                onChange={(e) => setMaxAttendees(Number(e.target.value))}
                value={maxAttendees}
                id="maxAttendees"
                autoComplete="on"
                type="number"
                shadow
              />
            </div>
            <Button className="mt-2" type="submit" to={`api/event/${id}`}>
              Update
            </Button>
          </form>
          <Button
            type="submit"
            className="w-full"
            outline
            onClick={handleSubmit}
          >
            cancel
          </Button>
        </div>
      </div>
    </>
  );
}
