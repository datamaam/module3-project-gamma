import { useState } from "react";
// import "./EventForm.css";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { TextInput, Button, Label, Checkbox } from "flowbite-react";
import { useNavigate } from "react-router-dom";
import { useUserContext } from "./providers/UserProvider.js";

function EventForm() {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [location, setLocation] = useState("");
  const [startTime, setStartTime] = useState("");
  const [endTime, setEndTime] = useState("");
  const [eventImage, setEventImage] = useState("");
  const [isPublic, setIsPublic] = useState(true);
  const [maxAttendees, setMaxAttendees] = useState(0);
  const { token } = useToken();
  const { user } = useUserContext();
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const eventData = {};
    eventData.name = name;
    eventData.description = description;
    eventData.location = location;
    eventData.start_time = new Date(startTime).toISOString();
    eventData.end_time = new Date(endTime).toISOString();
    eventData.event_image = eventImage;
    eventData.is_public = isPublic;
    eventData.max_attendees = maxAttendees;


    const eventUrl = `${process.env.REACT_APP_API_HOST}/api/events`;
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(eventData),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      credentials: "include"
    };

    const eventResponse = await fetch(eventUrl, fetchConfig);
    if (eventResponse.ok) {
      const eventInfo = await eventResponse.json();

      const attendeeUrl = `${process.env.REACT_APP_API_HOST}/api/events/${eventInfo.id}/attendees`
      const attendeeFetchConfig = {
        method: "post",
        body: JSON.stringify({
          event_id: eventInfo.id,
          user_id: user.id,
        }),
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        credentials: "include",
      };

      const attendeeResponse = await fetch(attendeeUrl, attendeeFetchConfig)

      if (attendeeResponse.ok) {
        navigate(`/my-events`)
        setName("");
        setDescription("");
        setLocation("");
        setStartTime("");
        setEndTime("");
        setEventImage("");
        setIsPublic(true);
        setMaxAttendees(0);
      }
      }

  };
  const handleNameChange = (e) => {
    const value = e.target.value;
    setName(value);
  };
  const handleDescriptionChange = (e) => {
    const value = e.target.value;
    setDescription(value);
  };
  const handleLocationChange = (e) => {
    const value = e.target.value;
    setLocation(value);
  };
  const handleStartTimeChange = (e) => {
    const value = e.target.value;
    setStartTime(value);
  };
  const handleEndTimeChange = (e) => {
    const value = e.target.value;
    setEndTime(value);
  };
  const handleEventImageChange = (e) => {
    const value = e.target.value;
    setEventImage(value);
  };
  const handleIsPublicChange = (e) => {
    const value = e.target.checked;
    setIsPublic(value);
  };
  const handleMaxAttendeesChange = (e) => {
    const value = e.target.value;
    setMaxAttendees(value);
  };

  return (
    <>
      <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-sm">
          <h2 className="text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
            Host an Event!
          </h2>
        </div>
        <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm space-y-6">
          <form
            onSubmit={handleSubmit}
            id="create-item-form"
            className="flex max-w-md flex-col gap-4"
          >
            <div className="mb-4 block">
              <Label
                htmlFor="base-input"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Event Name
              </Label>
              <TextInput
                onChange={handleNameChange}
                value={name}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
              />
            </div>
            <div className="mb-4 block">
              <Label
                htmlFor="base-input"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Event Description
              </Label>
              <TextInput
                onChange={handleDescriptionChange}
                value={description}
                placeholder="Description"
                type="text"
                name="description"
                id="description"
              />
            </div>
            <div className="mb-4 block">
              <Label
                htmlFor="base-input"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Event Location
              </Label>
              <TextInput
                onChange={handleLocationChange}
                value={location}
                placeholder="Location"
                type="text"
                name="Location"
                id="Location"
              />
            </div>
            <div className="mb-4 block">
              <Label
                htmlFor="base-input"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Event Start Time
              </Label>
              <TextInput
                onChange={handleStartTimeChange}
                value={startTime}
                placeholder=""
                required
                type="datetime-local"
                name="startTime"
                id="startTime"
              />
            </div>
            <div className="mb-4 block">
              <Label
                htmlFor="base-input"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Event End Time
              </Label>
              <TextInput
                onChange={handleEndTimeChange}
                value={endTime}
                placeholder=""
                required
                type="datetime-local"
                name="endTime"
                id="endTime"
              />
            </div>
            <div className="mb-4 block">
              <Label
                htmlFor="base-input"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Event Image
              </Label>
              <TextInput
                onChange={handleEventImageChange}
                value={eventImage}
                placeholder="Image"
                type="url"
                name="eventImage"
                id="eventImage"
              />
            </div>
            <div className="flex items-center gap-2">
              <Label htmlFor="base-input" className="flex mt-1">
                Is this a public event?
              </Label>
              <Checkbox
                onChange={handleIsPublicChange}
                value={isPublic}
                placeholder=""
                type="checkbox"
                name="isPublic"
                id="isPublic"
              />
            </div>
            <div className="mb-4 block">
              <Label
                htmlFor="base-input"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Max Attendees
              </Label>
              <TextInput
                onChange={handleMaxAttendeesChange}
                value={maxAttendees}
                placeholder="Max Attendees"
                type="number"
                name="maxAttendees"
                id="maxAttendees"
                min="0"
              />
            </div>
            <Button outline gradientDuoTone="pinkToOrange" type="submit">
              Create your event!
            </Button>
          </form>
        </div>
      </div>
    </>
  );
}

export default EventForm;
