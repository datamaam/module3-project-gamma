import React from "react";

function AttendeeList({ attendees }) {
    return (
        <>
        <div className="flex justify-center mb-4 sm:mx-auto sm:w-full sm:max-w-sm">
            <h1 className="mt-10 text-center text-3xl font-bold leading-9 tracking-tight text-gray-900">
            Attendees
            </h1>
        </div>
        <div className="flex justify-center mb-4 sm:mx-auto sm:w-full sm:max-w-sm overflow-auto">
            <ul className="divide-y divide-gray-100">
            {attendees.map((attendee) => {
                return (
                <li key={attendee.id} className="flex gap-x-4 py-5">
                    <img
                    className="h-12 w-12 flex-none rounded-full bg-gray-50"
                    src={attendee.profile_image}
                    alt=""
                    />
                    <div className="min-w-0">
                    <p className="text-sm font-semibold leading-6 text-gray-900">
                        {attendee.first_name} {attendee.last_name}
                    </p>
                    </div>
                </li>
                );
            })}
            </ul>
        </div>
        </>
    );
}

export default AttendeeList;
