import { Outlet, Navigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";

const PrivateRoutes = () => {
  const { token } = useToken();
  return token ? <Outlet /> : <Navigate to="/login" />;
};

export default PrivateRoutes;
