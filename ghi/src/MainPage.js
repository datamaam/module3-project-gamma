import { Button } from "flowbite-react";
import { useNavigate } from "react-router-dom";
import wishbone from "./assets/potlucky-icon.svg";
import { useUserContext } from "./providers/UserProvider.js";

function MainPage() {
  const navigate = useNavigate();
  const { user } = useUserContext();

  const handleSignup = () => {
    navigate(`/signup`);
  };

  return (
    <>
      <div className="bg-mainhero bg-cover bg-center h-screen">
        <div className="relative isolate px-6 py-28 md:py-40 lg:py-40">
          <div className="mx-auto max-w-2xl p-14 pb-12 bg-white rounded-2xl">
            <div className="flex content-center items-center mx-auto bg-yellow-300 p-5 rounded-full w-20 h-20 mb-8 -m-24">
              <img
                src={wishbone}
                className="h-10 mx-auto"
                alt="Potlucky wishbone logo"
              />
            </div>
            {/* <div className="hidden mb-8 sm:flex sm:justify-center"></div> */}
            <div className="text-center item">
              <h2 className="text-5xl font-bold tracking-tight text-gray-900">
                Effortless Potluck Planning
              </h2>
              <p className="mt-8 text-lg leading-8 text-gray-600">
                Effortlessly organize your next gathering with Potlucky. Simply
                create your event, invite guests, and let everyone sign up to
                bring their favorite dishes. Say goodbye to confusion and hello
                to stress-free potlucks with Potlucky.
              </p>
              <div className="mt-9 flex items-center justify-center gap-x-5">
                {user ? (
                  <>
                    <Button
                      onClick={() => navigate("/my-events")}
                      gradientDuoTone="pinkToOrange"
                      className="w-full py-2"
                    >
                      My events
                    </Button>
                    <Button
                      onClick={() => navigate("/events/public")}
                      gradientDuoTone="pinkToOrange"
                      className="w-full py-2"
                    >
                      Public events
                    </Button>{" "}
                  </>
                ) : (
                  <>
                    <Button
                      onClick={handleSignup}
                      gradientDuoTone="pinkToOrange"
                      className="w-full py-2"
                    >
                      Sign Up
                    </Button>
                    <Button
                      onClick={() => navigate("/login")}
                      gradientDuoTone="pinkToOrange"
                      className="w-full py-2"
                    >
                      Log in
                    </Button>{" "}
                  </>
                )}
              </div>
            </div>
          </div>
          <div
            className="absolute inset-x-0 top-[calc(100%-13rem)] -z-10 transform-gpu overflow-hidden blur-3xl sm:top-[calc(100%-30rem)]"
            aria-hidden="true"
          ></div>
        </div>
      </div>
    </>
  );
}

export default MainPage;
