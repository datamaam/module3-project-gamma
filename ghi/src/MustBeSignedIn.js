import { Button } from "flowbite-react";
import { useNavigate } from "react-router-dom";

export default function MustBeSignedIn() {
  const navigate = useNavigate();
  return (
    <>
      <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8 mt-5">
        <div className="sm:mx-auto sm:w-full sm:max-w-sm">
          <h2 className="text-lg text-center mb-5 font-semibold">
            You must be signed in to view an event.
          </h2>
          <div className="mt-8 gap-x-5">
            <Button
              onClick={() => navigate("/login")}
              gradientDuoTone="pinkToOrange"
              className="w-full"
            >
              Log in
            </Button>
            <Button
              onClick={() => navigate("/signup")}
              gradientDuoTone="pinkToOrange"
              className="w-full mt-5"
            >
              Sign Up
            </Button>
          </div>
        </div>
      </div>
    </>
  );
}
