import React, { useEffect, useState } from "react";
import { Card } from "flowbite-react";
import { Link } from "react-router-dom";
import defaultbg from "./assets/defaultbg.jpg";

function PublicEvent() {
  const [events, setEvents] = useState([]);

  const fetchPublicEvents = async () => {
    const url = `${process.env.REACT_APP_API_HOST}/api/events/public`;
    const fetchConfig = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (!response.ok) throw new Error("Failed to fetch events.");
      const data = await response.json();
      setEvents(data);
    } catch (e) {
      console.error("Error fetching public events:", e);
    }
  };

  useEffect(() => {
    fetchPublicEvents();
  }, []);
  const filteredEvents = events.filter(
    (event) => new Date(event.start_time) >= Date.now()
  );
  return (
    <>
      <h1 className="mt-10 text-center text-3xl font-bold leading-9 tracking-tight text-gray-900">
        Public Events
      </h1>
      <div className="flex flex-wrap justify-center my-10">
        {filteredEvents.length > 0 ? (
          filteredEvents.map((event) => (
            <Card
              key={event.id}
              imgAlt={event.name}
              imgSrc={event.event_image || defaultbg}
              className="max-w-sm w-full md:w-1/3 lg:w-1/4 p-4 m-2"
            >
              <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white mb-2">
                <Link to={`/event/${event.id}`}>{event.name}</Link>
              </h5>
              <p className="font-normal text-gray-700 dark:text-gray-400 mb-4">
                {new Date(event.start_time).toLocaleString()}
              </p>
            </Card>
          ))
        ) : (
          <p>No events to display</p>
        )}
      </div>
    </>
  );
}

export default PublicEvent;
