import React, { useEffect, useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useParams, useNavigate } from "react-router-dom";
import { TextInput, Button, Textarea, Label, Select } from "flowbite-react";
function ItemForm() {
  const [catagories, setCat] = useState([]);
  const { token } = useToken();
  const { id } = useParams();
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    name: "",
    description: "",
    quantity: 1,
    item_image: "",
    event_id: id,
    category_id: 1,
  });
  const fetchData = async () => {
    const url = `${process.env.REACT_APP_API_HOST}/api/categories`;
    console.log(url);
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setCat(data);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);
  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };
  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = `${process.env.REACT_APP_API_HOST}/api/items`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      credentials: "include",
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      navigate(-1);
      setFormData({
        name: "",
        description: "",
        quantity: 0,
        item_image: "",
        event_id: id,
        category_id: null,
      });

    }
  };
  // _________________________________________________________________________________________________
  return (
    <>
      <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-sm">
          <h2 className="text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
            Bring an Item!
          </h2>
        </div>
        <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm space-y-6">
          <form
            onSubmit={handleSubmit}
            id="create-item-form"
            className="flex max-w-md flex-col gap-4">
            <div className="mb-4 block">
              <Label
                htmlFor="base-input"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Item Name
              </Label>
              <TextInput
                onChange={handleFormChange}
                value={formData.name}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
              />
            </div>
            <div className="mb-4 block">
              <Label
                htmlFor="base-input"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Item Description
              </Label>
              <Textarea
                onChange={handleFormChange}
                value={formData.description}
                placeholder="Description"
                required
                type="text"
                name="description"
                id="description"
              />
            </div>
            <div className="mb-4 block">
              <Label
                htmlFor="base-input"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                How many are you bringing?
              </Label>
              <TextInput
                onChange={handleFormChange}
                value={formData.quantity}
                placeholder="Quantity"
                required
                type="number"
                name="quantity"
                id="quantity"
                min="1"
              />
            </div>
            <div className="mb-4 block">
              <Label
                htmlFor="base-input"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Please leave an image link here
              </Label>
              <TextInput
                onChange={handleFormChange}
                value={formData.item_image}
                placeholder="Image link"
                type="text"
                name="item_image"
                id="item_image"
              />
            </div>
            <div className="mb-4 block">
              <Label
                htmlFor="base-input"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
                Select a Category
              </Label>
              <Select
                onChange={handleFormChange}
                value={formData.category_id}
                required
                name="category_id"
                id="category_id">
                {catagories.map((cat) => {
                  return (
                    <option key={cat.id} value={cat.id}>
                      {cat.name}
                    </option>
                  );
                })}
              </Select>
            </div>
            <Button gradientDuoTone="pinkToOrange" type="submit">
              Add Item
            </Button>
          </form>
        </div>
      </div>
    </>
  );
}
export default ItemForm;
