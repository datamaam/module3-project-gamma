/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
    "node_modules/flowbite-react/lib/esm/**/*.js",
  ],
  theme: {
    extend: {
      backgroundImage: {
        mainhero: "url('../src/assets/food-table.jpg')",
      },
      colors: {},
    },
  },
  plugins: [require("flowbite/plugin")],
};
