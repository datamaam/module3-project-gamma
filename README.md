# Potlucky

Effortlessly organize your next gathering with Potlucky. Simply create your event, invite guests with a link to the event, and let everyone sign up to bring their favorite dishes. Say goodbye to confusion and hello to stress-free potlucks with Potlucky.

Team members:
- Amy Pirro
- Daniel Gay
- Devonte Young
- Matthew Hanson

[See the deployed site](https://datamaam.gitlab.io/Potlucky/)

## Design

- [APIs](docs/apis.md)
- [Data models](docs/data-models.md)
- [GHI/Wireframes](docs/GHI.md)

## Run Potlucky locally

To run Potlucky locally, please ensure Docker Desktop is installed on your local machine and follow these steps:

1. Clone this repository
2. CD into the repo directory
3. Run `docker volume create potlucky-db`
4. Run `docker-compose build`
5. Run `docker-compose up`
6. In Docker Desktop, in the potlucky-fastapi-1 container, on Exec tab, run `python -m migrations up`. <br>Alternatively: in your terminal, run `docker exec -it potlucky-fastapi-1 bash` and then `python -m migrations up`
7. Go to localhost:3000 in your browser
8. Sign up as a new user and explore creating events, adding items to events, and more.