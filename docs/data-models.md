# Potlucky Data Models

## Users

| name          | type       | unique | optional |
|---------------|------------|:------:|:--------:|
| id            | serial/int |    ✔︎   |          |
| email         | string     |    ✔︎   |          |
| password      | string     |        |          |
| first_name    | string     |        |          |
| last_name     | string     |        |          |
| phone_number  | string     |    ✔︎   |          |
| bio           | string     |        |     ✔︎    |
| profile_image | string     |        |     ✔︎    |


## Events

| name          | type              | unique | optional |
|---------------|-------------------|:------:|:--------:|
| id            | serial/int        |    ✔︎   |          |
| name          | string            |        |          |
| description   | string            |        |     ✔︎    |
| location      | string            |        |     ✔︎    |
| start_time    | datetime          |        |          |
| end_time      | datetime          |        |     ✔︎    |
| event_image   | string            |        |     ✔︎    |
| is_public     | bool              |        |          |
| max_attendees | int               |        |     ✔︎    |
| owner_id      | int ref users(id) |        |          |

Notes:

- `owner_id` is optional because we weren't sure if we wanted to delete past events if the event owner's account was deleted. Functionally, the id of the logged-in user who creates the event is assigned to `owner_id`.

## Categories

| name    | type              | unique | optional |
|---------|-------------------|:------:|:--------:|
| id      | serial/int        |    ✔︎   |          |
| name    | string            |        |          |
| user_id | int ref users(id) |        |     ✔︎    |

Notes:

- `user_id` is present and optional because originally we envisioned that users could add their own categories to events, and when creating new events later, the categories associated with them would be suggested (along with default categories that have null `user_id`, which are ones we've hard-coded in; see below).
- Currently, we have seven hard-coded categories with null `user_id`:
  - Entrees
  - Sides
  - Desserts
  - Supplies
  - Misc.
  - Drinks
  - Games


## Items

| name        | type                   | unique |            optional           |
|-------------|------------------------|:------:|:-----------------------------:|
| id          | serial/int             |    ✔︎   |                               |
| name        | string                 |        |                               |
| description | string                 |        |               ✔︎               |
| quantity    | int                    |        | defaults to 1 if not included |
| item_image  | string                 |        |               ✔︎               |
| user_id     | int ref users(id)      |        |               ✔︎               |
| event_id    | int ref events(id)     |        |                               |
| category_id | int ref categories(id) |        |               ✔︎               |

Notes:

- `user_id` is optional because originally we envisioned that event owners could add "suggested" items that attendees could claim. However, functionally at this point the logged-in user's id is assigned to `user_id` when they create an item.
- `category_id` is optional for the same reason


## Attendees

Junction table (many attendees, many events). Both `event_id` and `user_id` have ON DELETE CASCADE — if an event is deleted, all attendees of that event are deleted (by deleting the rows referencing that event on this table), and if a user is deleted, they are removed as attendees from all events they've joined (by deleting the rows referencing that user on this table).

| name     | type               | unique | optional |
|----------|--------------------|:------:|:--------:|
| event_id | int ref events(id) |        |          |
| user_id  | int ref users(id)  |        |          |


## Event_categories

Junction table (many events, many categories). Both have ON DELETE CASCADE, though possibly category_id should not (because it would affect past events).

| name        | type                   | unique | optional |
|-------------|------------------------|:------:|:--------:|
| event_id    | int ref events(id)     |        |          |
| category_id | int ref categories(id) |        |          |

Notes:

- This table exists because eventually we'd like event owners to be able to create and/or choose their own categories for an event. Currently we have seven hard-coded categories which are all generated on the front end for each created event, and this table isn't utilized.