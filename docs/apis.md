# APIs

## Event

- **Method**: `POST`, `GET`, `GET`, `PUT`, `DELETE`
- **Path**: `/api/events`, `/api/events/public`, `/api/events/{event_id}`

Input:

````json
  {
  "name": "string",
  "description": "string",
  "location": "string",
  "start_time": "2024-02-08T19:59:11.643Z",
  "end_time": "2024-02-08T19:59:11.643Z",
  "event_image": "string",
  "is_public": true,
  "max_attendees": 0
}

    ```

Output:

    ```json
    {
  "id": 0,
  "name": "string",
  "description": "string",
  "location": "string",
  "start_time": "2024-02-08T19:59:40.170Z",
  "end_time": "2024-02-08T19:59:40.170Z",
  "event_image": "string",
  "is_public": true,
  "max_attendees": 0,
  "owner_id": 0
}
    ```


Creating an event in our system involves entering details such as name, description, location, time, and image URL, along with specifying whether the event is public and the number of attendees. When you submit, the system assigns a unique ID, checks the time, and links it to the author's ID. This process allows you to easily identify events and get clear information about engagement. Features like image URLs and attendee limits enhance the user experience, making it suitable for both public and private meetings.

## User

- **Method**: `POST`, `GET`, `GET`, `GET`, `PUT`, `DELETE`
- **Path**: `/token`, `/api/users`, `/api/users/{user_id}`

Input:

```json

{
  "email": "string",
  "password": "string",
  "first_name": "string",
  "last_name": "string",
  "phone_number": "string",
  "bio": "string",
  "profile_image": "string"
}

```
Output:

```json
{
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
    "id": 0,
    "email": "string",
    "first_name": "string",
    "last_name": "string",
    "phone_number": "string",
    "bio": "string",
    "profile_image": "string"
  }
}
```

To create a user in our system, submit a POST request with the user's email, password, personal information, and profile image URL. Upon creation, the system provides user account details including an access token and unique ID and submitted information. This process ensures secure and personalized user account creation, improving user interaction and account management.


## Categories

- Method: `GET`, `GET`,
- Path: `/api/categories`, `/api/categories/{category_id}`

Input:

```json
[
  {
    "id": 0,
    "name": "string"
  }
]
```

Output:

```json
[
  {
    "id": 0,
    "name": "string"
  }
]
```

The categories shows you all categories that can be in a event. Entree, sides, dessert, drink, games, supplies and misc. each with a unique ID and name. This categorization helps in organizing and identifying event needs efficiently.


## Items

- Method: `POST`, `GET`, `GET`, `PUT`, `DELETE`
- Path: `/api/items`, `/api/items/{item_id}`, `/api/events/{event_id}/items`

Input:

```json
{
  "name": "string",
  "description": "string",
  "quantity": 0,
  "item_image": "string",
  "event_id": 0,
  "category_id": 0
}
```

Output:

```json
{
  "id": 0,
  "name": "string",
  "description": "string",
  "quantity": 0,
  "item_image": "string",
  "user": {
    "id": 0,
    "first_name": "string",
    "last_name": "string",
    "profile_image": "string"
  },
  "event_id": 0,
  "category": {
    "id": 0,
    "name": "string"
  }
}
```

Use POST to add items to an event, providing details like name, description, and quantity. Use GET to fetch, PUT to update, and DELETE to remove items.

## Attendees

- Method: `POST`, `GET`, `DELETE`
- Path: `/api/events/{event_id}/attendees`, `/api/users/{user_id}`

Input:

```json
{
  "question": string,
  "answers": string
}
```

Output:

```json
{
  "id": int,
  "questions": string,
  "answers": string
}
```

Quiz questions and answers will be populated by employees and will largely remain the same. However, to create and update questions and answers should both be updated so the imput and outputs will be very similar.

## Quizzes

- Method: `POST`, `DELETE`
- Path: `/api/body-quiz`, `/api/home-quiz`

Input:

```json
{
  "responses": string
}
```

Output:

```json
{
  "user": string,
  "questions": string,
  "responses": string,
  "quiz_date": datetime
}
```

An instance of a quiz will be created when a user takes the quiz, users will only provide their answers, the output will save answers associated to their questions, the user who took the quiz, and the date/time the quiz was taken. Users with accounts can delete saved quiz results.

## Create a cart

- **Method**: `GET`, `POST`, `DELETE`, `UPDATE`
- **Path**: `/api/cart/<int:pk>`

Input:

```json
{
  "product": string,
  "quantity": int,
  "totals": int,
  "created": date
}
```

Output:

```json
{
  "product": string,
  "quantity": int,
  "totals": int,
  "created": date
}
```

Create a new cart that uses the product information to calculate the total price of all the products in the cart

## Orders

- **Method**: `POST`, `GET`, `GET`, `PUT`, `DELETE`
- **Path**: `/api/orders`, `/api/orders/<int:pk>`

Input:

```json
{
  "products": {
    "name": string,
    "sku": string,
    "size": string,
    "price": int
  },
  "price": int,
  "quantity": int
}
```

Output:

```json
{
  "products": {
    "name": string,
    "sku": string,
    "size": string,
    "price": int
  },
  "price": int,
  "quantity": int,
  "total": int,
  "order_number": string,
  "customer": string
}
```

Creating a new order collects all of the relevant product data from the order, and matches the quantities to those prices. It will then calculate the subtotal(s) and total. The order number will be generated serially. A query is also made to match the order to the customer who made the order, placing their name or id in the result.

NOTE: Orders should describe a ProductVO as opposed to the Product entity. The ProductVO will also have a "quantities" prop on it. Then, Orders price and quantity is much more easily calculated and is not manipulating Products directly. We will likely find other value objects as we go through this.

````
