### view an event

* Endpoint path: /events/{event_id}
* Endpoint method: GET

* Headers:
  * Authorization: Bearer token

* Response: single event view
* Response shape (JSON):
    ```json
    {
        "name": string
        "description": string
        "location": string
        "start_time": date_time
        "end_time": date_time
        "event_image": url
        "is_public": boolean
        "max_attendees": integer
        "categories": foreign key
        "attendees": foreign key
        "id": integer
    }
    ```

### create events

* Endpoint path: /events
* Endpoint method: POST

* Headers:
  * Authorization: Bearer token

* Request shape (JSON):
    ```json
    {
        "name": string
        "description": string
        "location": string
        "start_time": date_time
        "end_time": date_time
        "event_image": url
        "is_public": boolean
        "max_attendees": integer
        "categories": foreign key
        "attendees": foreign key
    }

    ```

* Response: event created
* Response shape (JSON):
    ```json
    {
        "name": string
        "description": string
        "location": string
        "start_time": date_time
        "end_time": date_time
        "event_image": url
        "is_public": boolean
        "max_attendees": integer
        "categories":[{
            "name": string
        }]

        "attendees": [{
            "id": integer
            "username": string
            "profile_image": url
        }]

        "id": integer
    }
    ```

### update events

* Endpoint path: /events/{event_id}
* Endpoint method: PUT

* Headers:
  * Authorization: Bearer token

* Request shape (JSON):
    ```json
    {
        "name": string
        "description": string
        "location": string
        "start_time": date_time
        "end_time": date_time
        "event_image": url
        "is_public": boolean
        "max_attendees": integer

    }

    ```

* Response: event updated
* Response shape (JSON):
    ```json
    {
        "name": string
        "description": string
        "location": string
        "start_time": date_time
        "end_time": date_time
        "event_image": url
        "is_public": boolean
        "max_attendees": integer
        "categories":[{
            "name": string
        }]

        "attendees": {
            "id": integer
            "username": string
            "profile_image": url

        }

        "id": integer
    }
    ```

### view all events

* Endpoint path: /events
* Endpoint method: GET

* Query parameters:
  * q: search for other events

* Headers:
  * Authorization: Bearer token

* Response: all event view
* Response shape (JSON):
    ```json
    {
       "name": string
        "description": string
        "location": string
        "start_time": date_time
        "end_time": date_time
        "event_image": url
        "is_public": boolean
        "max_attendees": integer
        "categories":[{
            "name": string
        }]

        "attendees": [{
            "id": integer
            "username": string
            "profile_image": url
        }]

        "id": integer
    }
    ```
### event deleted

* Endpoint path: /events/{event_id}
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token


* Response: success or failure of deleting event
* Response shape (JSON):
    ```json
    {
        "granted": boolean
        "message": string
    }
    ```
