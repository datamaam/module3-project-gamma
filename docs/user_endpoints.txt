### Log in

* Endpoint path: /token
* Endpoint method: POST

* Request shape (form):
  * username: string
  * password: string

* Response: Account information and a token
* Response shape (JSON):
    ```json
    {
      "account": {
        «key»: type»,
      },
      "token": string
    }
    ```


### Log out

* Endpoint path: /token
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: Always true
* Response shape (JSON):
    ```json
    true
    ```


### Sign-up

* Endpoint path: /signup
* Endpoint method: POST


* Request shape (JSON):
    ```json
    {
        "username": string,
        "first_name": string,
        "fast_name": string,
        "password": string,
        "phone_number": string,
        "email": string

    }
    ```

* Response: Creating user account
* Response shape (JSON):
    ```json
    {
        "granted": boolean,
        "message": string
    }
    ```

### users profile view

* Endpoint path: /users/{username}
* Endpoint method: GET

* Headers:
  * Authorization: Bearer token


* Response: User Profile Page view
* Response shape (JSON):
    ```json
    {
        "username": string,
        "first_name": string,
        "fast_name": string,
        "password": string,
        "phone_number": string,
        "email": string
        "profile_image": string
        "event_id": ?
        "id": integer
        "bio": string
    }
    ```

### users profile update

* Endpoint path: /users/{username}
* Endpoint method: PUT

* Headers:
  * Authorization: Bearer token

* Request shape (JSON):
    ```json
    {
       "username": string,
        "first_name": string,
        "fast_name": string,
        "password": string,
        "phone_number": string,
        "email": string
        "profile_image": string
        "bio": string
    }

* Response: User profile updated
* Response shape (JSON):
    ```json
    {
        "username": string,
        "first_name": string,
        "fast_name": string,
        "password": string,
        "phone_number": string,
        "email": string
        "profile_image": string
        "event_id": ?
        "id": integer
        "bio": string
    }
    ```



### users profile delete

* Endpoint path: /users/{username}
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token


* Response: success or failure of deleting user
* Response shape (JSON):
    ```json
    {
        "granted": boolean
        "message": string
    }
    ```


### list of users

* Endpoint path: /users
* Endpoint method:GET

* Headers:
  * Authorization: Bearer token

* Response: List of users
* Response shape (JSON):
    ```json
    {
        "username": string,
        "first_name": string,
        "fast_name": string,
        "phone_number": string,
        "email": string
        "profile_image": string
        "id": integer
        "event_id": string
        "bio": string
    }
    ```
